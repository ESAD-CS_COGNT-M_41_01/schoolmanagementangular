import { Component } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "./Service/auth.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor(private router: Router,private auth : AuthService){}
  isLoggedIn = this.auth.authenticated;


  logout(){
    this.auth.clear();
    this.router.navigateByUrl("/")
  }
  // public username: string;
  // public password: string;
  // public errorMessage: string;
  // constructor(private router: Router) { }
  // authenticate(form: NgForm) {
  //   if (form.valid) {
  //     // perform authentication
  //     this.router.navigateByUrl("/admin/main");
  //   } else {
  //     this.errorMessage = "Form Data Invalid";
  //   }
  // }

}
