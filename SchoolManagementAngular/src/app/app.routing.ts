import { Routes, RouterModule } from "@angular/router";
import { loginComponent } from "./login/login.component";
import { AppComponent } from "./app.component";
import { NotFoundComponent } from "./login/NotFoundComponent";
import { AuthGuard } from "./Service/auth.guard";
import { HomeComponent } from "./home/home.component";
import { AdminComponent } from "./admin/admin.component";
import { BranchAdminComponent } from "./branch-admin/branch-admin.component";
import { StudentComponent } from "./student/student.component";
import { TeacharComponent } from "./teachar/teachar.component";
import { StaffComponent } from "./staff/staff.component";


const routes: Routes = [
{ path: "Home", component: HomeComponent},
{ path: "Admin", component: AdminComponent,  canActivate: [AuthGuard]},
{ path: "BranchAdmin", component: BranchAdminComponent,  canActivate: [AuthGuard]},
{ path: "Student", component: StudentComponent,  canActivate: [AuthGuard]},
{ path: "Teacher", component: TeacharComponent,  canActivate: [AuthGuard]},
{ path: "Staff", component: StaffComponent,  canActivate: [AuthGuard]},
{ path: "login", component: loginComponent },
{ path: "", redirectTo: "/Home", pathMatch: "full" },
{ path: "**", component: NotFoundComponent }
]


export const routing = RouterModule.forRoot(routes);