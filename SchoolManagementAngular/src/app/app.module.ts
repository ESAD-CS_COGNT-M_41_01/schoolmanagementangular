import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {  HttpClientModule } from '@angular/common/http';
import { RestDataSource } from './Service/data.Service';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AuthService } from './Service/auth.service';
import { loginComponent } from './login/login.component';

import { routing } from "./app.routing";
import { NotFoundComponent } from './login/NotFoundComponent';
import { AuthGuard } from './Service/auth.guard';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { StudentComponent } from './student/student.component';
import { TeacharComponent } from './teachar/teachar.component';
import { StaffComponent } from './staff/staff.component';
import { BranchAdminComponent } from './branch-admin/branch-admin.component';

@NgModule({
  declarations: [
    AppComponent, loginComponent, NotFoundComponent, HomeComponent, 
    AdminComponent, StudentComponent, TeacharComponent, StaffComponent, 
    BranchAdminComponent
  ],
  imports: [
    BrowserModule, HttpClientModule,RouterModule, FormsModule, routing
  ],
  providers: [RestDataSource, AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
